package cat.dam.edgar.trucada;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Button> numbers;
    private Button btn_sms, btn_call, btn_del;
    private EditText et_sms;
    private TextView tv_numTel;
    private final int MY_PERMISSIONS_REQUEST_CALL=1;
    private final int MY_PERMISSIONS_REQUEST_SMS=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        tv_numTel = (TextView) findViewById(R.id.tv_num);
        et_sms = (EditText) findViewById(R.id.et_sms);
        buttonsVar();
    }

    private void buttonsVar()
    {
        btn_sms = (Button) findViewById(R.id.btn_sms);
        btn_call = (Button) findViewById(R.id.btn_call);
        btn_del = (Button) findViewById(R.id.btn_del);
        numbersVar();
    }

    private void numbersVar()
    {
        int btnId;
        String idName = "btn_num", temp;
        numbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            temp = idName + i;
            btnId = getResources().getIdentifier(temp, "id", this.getPackageName());
            numbers.add(findViewById(btnId));
        }
    }

    private void setListeners()
    {
        numbersListener();
        callListener();
        smsListener();
        delListener();
    }

    private void numbersListener()
    {
        for (Button btn_num : numbers) {
            btn_num.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv_numTel.getText().length() == 9) createToast(getResources().getString(R.string.limit_exceed));
                    else addNumber(btn_num);
                }
            });
        }
    }

    private void callListener()
    {
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nTel = tv_numTel.getText().toString();

                if (nTel.isEmpty()) createToast(getResources().getString(R.string.empty));
                else callTel(nTel);
            }
        });
    }

    private void smsListener()
    {
        btn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = et_sms.getText().toString();
                String nTel = tv_numTel.getText().toString();

                if (message.isEmpty()) createToast(getResources().getString(R.string.empty_sms));
                else makeSms(message, nTel);
            }
        });
    }

    private void delListener()
    {
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nTel, temp = tv_numTel.getText().toString();
                int charDel = temp.length()-1;

                nTel = temp.substring(0, charDel);
                tv_numTel.setText(nTel);
            }
        });
    }

    private void callTel(String nTel)
    {
        if(haveTelPermission()) makeCall(nTel);
        else ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
    }

    @SuppressLint("MissingPermission") // Si no poso això es queixa perquè no comprovo el permís (ho comprovo en un altre mètode)
    private void makeCall(String nTel)
    {
        String dial = "tel:" + nTel;
        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));

    }

    private void addNumber(Button btn_num)
    {
        String tempText;

        try {
            tempText = tv_numTel.getText().toString();
        } catch (NullPointerException e){
            tempText = "";
        }
        tempText += btn_num.getText().toString();

        tv_numTel.setText(tempText);
    }

    private void makeSms(String message, String nTel)
    {
        if(haveSmsPermission()) sendSms(message, nTel);
        else ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SMS);
    }

    private void sendSms(String message, String nTel)
    {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(nTel, null, message, null, null);
    }

    private void createToast(String description)
    {
        Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CALL:
                if(!haveTelPermission()) createToast(getResources().getString(R.string.tel_denied));
                break;
            case MY_PERMISSIONS_REQUEST_SMS:
                if(!haveSmsPermission()) createToast(getResources().getString(R.string.sms_denied));
                break;
        }
    }

    private boolean haveTelPermission()
    {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean haveSmsPermission()
    {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED;
    }
}